package com.example.dialogflowv2;

import lombok.Data;

/**
 * @author Sadeghi Amir(sadeghi.h33@gmail.com)
 * @create on : 10/3/2019, Thu
 */
@Data
public class NlpParam {

    private String query;

    private String context;

    private String server;

    private String agent;
}
