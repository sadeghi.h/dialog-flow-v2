package com.example.dialogflowv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DialogflowV2Application {

    public static void main(String[] args) {
        SpringApplication.run(DialogflowV2Application.class, args);
    }

}
