package com.example.dialogflowv2;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by Mehdi Teymourlouie (mehdi.teymourlouie@gmail.com)
 * on 5/4/19.
 */
@Configuration
@Slf4j
public class GoogleCredentialConfig {

    private GoogleCredentials developRegular;

    private GoogleCredentials developRecurring;

    private GoogleCredentials productionRegular;

    private GoogleCredentials productionRecurring;

    @Value("${develop.agent.regular}")
    private String developRegularAgent;

    @Value("${develop.agent.recurring}")
    private String developRecurringAgent;

    public GoogleCredentialConfig() throws IOException, URISyntaxException {
        log.debug("google credential loaded....");

        URL regularDev = GoogleCredentialConfig.class.getResource("/sp-regular-staging-v2-dpjmlq-0e968aa68123.json");
        File regularDevCredentialsPath = new File(regularDev.toURI());
        try (FileInputStream serviceAccountStream = new FileInputStream(regularDevCredentialsPath)) {
            developRegular = ServiceAccountCredentials.fromStream(serviceAccountStream);
        } catch (FileNotFoundException ex) {
            log.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        URL recurringDev = GoogleCredentialConfig.class.getResource("/sp-recurrence-staging-v2-ylwse-ad3f7e2f2f75.json");
        File recurringDevCredentialsPath = new File(recurringDev.toURI());
        try (FileInputStream serviceAccountStream = new FileInputStream(recurringDevCredentialsPath)) {
            developRecurring = ServiceAccountCredentials.fromStream(serviceAccountStream);
        } catch (FileNotFoundException ex) {
            log.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    @Bean
    public GoogleCredentials developRegularCredential() {
        return developRegular;
    }

    @Bean
    public GoogleCredentials developRecurringCredential() {
        return developRecurring;
    }

    public String getDevelopRegularAgent() {
        return developRegularAgent;
    }

    public String getDevelopRecurringAgent() {
        return developRecurringAgent;
    }
}
