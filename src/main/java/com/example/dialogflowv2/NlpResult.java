package com.example.dialogflowv2;

import lombok.Data;

import java.util.List;

/**
 * @author Sadeghi Amir(sadeghi.h33@gmail.com)
 * @create on : 10/3/2019, Thu
 */
@Data
public class NlpResult {

    private String durationUnit;
    private String title;
    private String someDay;
    private String priority;
    private String dueDate;
    private String onDate;
    private String byDate;
    private String startDate;
    private String recurringStartDate;
    private String recurringEndDate;
    private String periodDate;
    private String onDateWkday;
    private String byDateWkday;
    private String dayPart;
    private String taskTime;
    private Float durationValue;
    private Integer taskXTimes;
    private Integer monthlyEndDay;
    private Integer monthlyStartDay;
    private String taskXTimePeriod;
    private String taskRepeat;
    private List<WeekDay> taskRepeatWeekDays;
}
