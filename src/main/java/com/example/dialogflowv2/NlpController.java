package com.example.dialogflowv2;

import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.dialogflow.v2.*;
import com.google.cloud.dialogflow.v2.TextInput.Builder;
import com.google.protobuf.ListValue;
import com.google.protobuf.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;

/**
 * @author Sadeghi Amir(sadeghi.h33@gmail.com)
 * @create on : 10/3/2019, Thu
 */
@RestController
@RequestMapping(NlpController.BASE_URL)
@Slf4j
public class NlpController {

    public static final String BASE_URL = "/dialogflow/v2";
    private final GoogleCredentialConfig credentialConfig;


    public NlpController(GoogleCredentialConfig credentialConfig) {
        this.credentialConfig = credentialConfig;
    }

    @PostMapping("/detectIntent")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<NlpResult> detectIntent(@RequestBody NlpParam nlpParam) throws Exception {
        String query = nlpParam.getQuery();
        GoogleCredentials credentials = null;
        String projectName = null;
        NlpResult nlpResult = new NlpResult();
        boolean isEmailContext = Objects.equals(nlpParam.getContext(), "email");
        if (nlpParam.getServer().compareToIgnoreCase("develop") == 0) {
            if (nlpParam.getAgent().compareToIgnoreCase("regular") == 0) {
                projectName = credentialConfig.getDevelopRegularAgent();
                credentials = credentialConfig.developRegularCredential();
            } else if (nlpParam.getAgent().compareToIgnoreCase("recurring") == 0) {
                projectName = credentialConfig.getDevelopRecurringAgent();
                credentials = credentialConfig.developRecurringCredential();
            }
        } else {
            //todo: for production
        }

        SessionsSettings sessionsSettings = SessionsSettings.newBuilder()
                .setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();

        try (SessionsClient sessionsClient = SessionsClient.create(sessionsSettings)) {
            SessionName session = SessionName.of(projectName, UUID.randomUUID().toString());
            Builder textInput = TextInput.newBuilder().setText(query).setLanguageCode("en-US");


            QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();
            QueryParameters queryParams;
            if (isEmailContext) {
                Context builderForValue = Context.newBuilder()
                        .setName(session.toString() + "/contexts/email")
                        .setLifespanCount(5).build();
                queryParams = QueryParameters.newBuilder().addContexts(builderForValue).build();

            } else {
                queryParams = QueryParameters.newBuilder().build();
            }
            DetectIntentRequest detectIntentRequest =
                    DetectIntentRequest.newBuilder()
                            .setSession(session.toString())
                            .setQueryInput(queryInput)
                            .setQueryParams(queryParams)
                            .build();
            DetectIntentResponse response = sessionsClient.detectIntent(detectIntentRequest);

            QueryResult queryResult = response.getQueryResult();
            Map<String, Value> nlpResponseMap = response.getQueryResult().getParameters().getFieldsMap();

            for (Map.Entry<String, Value> entry : nlpResponseMap.entrySet()) {
                String key = entry.getKey();
                Value value = entry.getValue();
                if (key.compareToIgnoreCase("task_duration") == 0) {

                    if (value.hasStructValue()) {
                        Map<String, Value> duration = value.getStructValue().getFieldsMap();
                        for (Map.Entry<String, Value> entry1 : duration.entrySet()) {
                            if (entry1.getKey().compareToIgnoreCase("time_unit") == 0) {
                                nlpResult.setDurationUnit(entry1.getValue().getStringValue());
                            }
                            if (entry1.getKey().compareToIgnoreCase("integer_part") == 0) {
                                nlpResult.setDurationValue((float) entry1.getValue().getNumberValue());
                            }
                        }
                    }
                }

                if (key.compareToIgnoreCase("task_title_original") == 0) {
                    nlpResult.setTitle(value.getStringValue());
                }
                if (key.compareToIgnoreCase("task_x_period") == 0) {
                    nlpResult.setTaskXTimePeriod(value.getStringValue());
                }
                if (key.compareToIgnoreCase("recurrstart") == 0) {
                    if (value.hasStructValue()) {
                        Map<String, Value> recurringStart = value.getStructValue().getFieldsMap();
                        for (Map.Entry<String, Value> entry1 : recurringStart.entrySet()) {
                            if (entry1.getKey().compareToIgnoreCase("task_start_date") == 0) {
                                nlpResult.setRecurringStartDate(entry1.getValue().getStringValue());
                            }
                        }
                    }
                }
                if (key.compareToIgnoreCase("recurrend") == 0) {
                    if (value.hasStructValue()) {
                        Map<String, Value> recurringEnd = value.getStructValue().getFieldsMap();
                        for (Map.Entry<String, Value> entry1 : recurringEnd.entrySet()) {
                            if (entry1.getKey().compareToIgnoreCase("task_end_date") == 0) {
                                nlpResult.setRecurringStartDate(entry1.getValue().getStringValue());
                            }
                        }
                    }
                }
                if (key.compareToIgnoreCase("monthly_start_day") == 0) {
                    nlpResult.setMonthlyStartDay((int) value.getNumberValue());
                }
                if (key.compareToIgnoreCase("monthly_end_day") == 0) {
                    nlpResult.setMonthlyEndDay((int) value.getNumberValue());
                }
                if (key.compareToIgnoreCase("task_x_times") == 0
                        && value.getNumberValue() > 0d) {
                    nlpResult.setTaskXTimes((int) value.getNumberValue());
                } else if (key.compareToIgnoreCase("task_repeat") == 0
                        && value.hasListValue()) {
                    ListValue values = value.getListValue();
                    Value repeatValue = values.getValues(0);
                    if (repeatValue.hasStructValue()) {
                        //used for week days recurring
                        List<WeekDay> weekDays = new ArrayList<>();
                        for (int i = 0; i < values.getValuesCount(); i++) {
                            Map<String, Value> weekDay = values.getValues(i).getStructValue().getFieldsMap();
                            WeekDay day = new WeekDay();
                            day.setWday(weekDay.get("wday").getStringValue());
                            weekDays.add(day);
                        }
                        nlpResult.setTaskRepeatWeekDays(weekDays);
                    } else {
                        nlpResult.setTaskRepeat(repeatValue.getStringValue());
                    }
                } else {

                    if (key.compareToIgnoreCase("task_time") == 0) {
                        nlpResult.setTaskTime(value.getStringValue());
                    }


                    if (key.compareToIgnoreCase("task_priority") == 0) {
                        nlpResult.setPriority(value.getStringValue());
                    }

                    if (key.compareToIgnoreCase("task_due_date") == 0) {
                        if (value.hasStructValue()) {
                            Map<String, Value> daueDate = value.getStructValue().getFieldsMap();
                            for (Map.Entry<String, Value> entry1 : daueDate.entrySet()) {
                                if (entry1.getKey().compareToIgnoreCase("task_due_date") == 0) {
                                    nlpResult.setDueDate(entry1.getValue().getStringValue());
                                }
                            }
                        }
                    }
                    if (key.compareToIgnoreCase("task_someday") == 0) {
                        nlpResult.setSomeDay(value.getStringValue());
                    } else if (key.compareToIgnoreCase("task_plan") == 0) {
                        if (value.hasStructValue()) {
                            Map<String, Value> plan = value.getStructValue().getFieldsMap();
                            for (Map.Entry<String, Value> entry1 : plan.entrySet()) {
                                if (entry1.getKey().compareToIgnoreCase("task_on_date") == 0) {
                                    nlpResult.setDueDate(entry1.getValue().getStringValue());
                                } else if (entry1.getKey().compareToIgnoreCase("task_on_date_wkday") == 0) {
                                    nlpResult.setOnDateWkday(entry1.getValue().getStringValue());
                                } else if (entry1.getKey().compareToIgnoreCase("task_by_date_wkday") == 0) {
                                    nlpResult.setByDateWkday(entry1.getValue().getStringValue());
                                } else if (entry1.getKey().compareToIgnoreCase("task_by_date") == 0) {
                                    nlpResult.setByDate(entry1.getValue().getStringValue());
                                } else if (entry1.getKey().compareToIgnoreCase("task_start_date") == 0) {
                                    nlpResult.setStartDate(entry1.getValue().getStringValue());
                                } else if (entry1.getKey().compareToIgnoreCase("task_by_period") == 0) {
                                    nlpResult.setPeriodDate(entry1.getValue().getStringValue());
                                }
                                if (entry1.getKey().compareToIgnoreCase("task_day_part") == 0) {
                                    nlpResult.setDayPart(entry1.getValue().getStringValue());
                                }
                            }
                        }
                    }

                }
            }
        } catch (IOException ex) {
        }
        return ResponseEntity.ok(nlpResult);
    }
}
