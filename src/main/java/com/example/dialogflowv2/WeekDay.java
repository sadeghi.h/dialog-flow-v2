package com.example.dialogflowv2;

import lombok.Data;

/**
 * @author Sadeghi Amir(sadeghi.h33@gmail.com)
 * @create on : 10/7/2019, Mon
 */
@Data
public class WeekDay {
    private String wday;
}
